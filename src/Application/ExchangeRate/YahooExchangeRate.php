<?php
/**
 * Created by PhpStorm.
 * User: siper
 * Date: 05.07.2017
 * Time: 04:04
 */

namespace Application\ExchangeRate;


use Application\Exception\CurrencyPairNotSupported;
use Money\Currency;


class YahooExchangeRate implements ExchangeRateProvider
{
    /**
     * @var Http
     */
    private $http_client;

    /**
     * #var array
     */
    private $rates;

    /**
     * YahooExchangeRate constructor.
     * @param $http_client
     */
    function __construct($http_client)
    {
        $this->http_client = $http_client;
        $this->rates = $this->pullFromYahoo();
    }

    /*
     * URL to Yahoo's finance API
     */
    const API_URL = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.xchange%20where%20pair%20in%20(%22GBPPLN%22%2C%22GBPEUR%22%2C%20%22PLNEUR%22%2C%20%22PLNGBP%22%2C%20%22EURPLN%22)%20&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback=";

    public function fetch(Currency $currencyIn, Currency $currencyOut)
    {

        if ($currencyIn->getCode() === 'GBP' && $currencyOut->getCode() === 'PLN') {
            return $this->rates['GBPPLN'];
        }

        if ($currencyIn->getCode() === 'GBP' && $currencyOut->getCode() === 'EUR') {
            return $this->rates['GBPEUR'];
        }

        if ($currencyIn->getCode() === 'PLN' && $currencyOut->getCode() === 'EUR') {
            return $this->rates['PLNEUR'];
        }

        if ($currencyIn->getCode() === 'PLN' && $currencyOut->getCode() === 'GBP') {
            return $this->rates['PLNGBP'];
        }

        if ($currencyIn->getCode() === 'EUR' && $currencyOut->getCode() === 'PLN') {
            return $this->rates['EURPLN'];
        }

        throw new CurrencyPairNotSupported();
    }

    /**
     * Pull rates from Yahoo and return it
     * @return Array
     */
    private function pullFromYahoo()
    {
        $data = $this->http_client->performRequest(self::API_URL, true);
        $rates = $data->query->results->rate;

        $out = [];

        foreach ($rates as $rate) {
            $out[ $rate->id ] = $rate->Rate;
        }

        return $out;
    }
}