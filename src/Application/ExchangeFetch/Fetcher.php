<?php
/**
 * Created by PhpStorm.
 * User: siper
 * Date: 03.07.2017
 * Time: 13:47
 */

namespace Application\ExchangeFetch;


use Application\ExchangeRate\ExchangeRateProvider;
use Money\Money;

/**
 * @property ExchangeRateProvider rate
 */
class Fetcher
{
    /**
     * @var ConvertParserProvider
     */
    private $parsed_string;

    /**
     * @var ExchangeRateProvider
     */
    private $rate;

    /**
     * @var float
     */
    private $current_rate;


    /**
     * Fetcher constructor.
     * @param ExchangeRateProvider $rate
     */
    function __construct(ExchangeRateProvider $rate)
    {
        $this->rate = $rate;
    }

    /**
     * Set string to parse
     * @param string $string
     */
    public function setStringToParse(string $string) {
        $this->parsed_string = ConvertParser::parse($string);

        $send = $this->parsed_string->getSendAmount();
        $currencyIn = $send->getCurrency();
        $currencyOut = $this->parsed_string->getReceiveCurrency();
        $this->current_rate = $this->rate->fetch($currencyIn, $currencyOut);
    }

    /**
     * @return Money
     */
    public function getSendAmount() {
        return $this->parsed_string->getSendAmount();
    }

    /**
     * Get Receive Amount
     * @return Money
     */
    public function getReceiveAmount() {
        $send = $this->parsed_string->getSendAmount();

        $currencyOut = $this->parsed_string->getReceiveCurrency();
        return new Money(round($send->getAmount() * $this->current_rate), $currencyOut);
    }

    /**
     * Get current rate
     * @return float
     */
    public function getCurrentRate() {
        return $this->current_rate;
    }
}