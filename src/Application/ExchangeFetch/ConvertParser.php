<?php
/**
 * Created by PhpStorm.
 * User: siper
 * Date: 01.07.2017
 * Time: 09:51
 */

namespace Application\ExchangeFetch;

use Application\ExchangeFetch\ConvertParser\BasicConvertParser;
use Application\ExchangeFetch\ConvertParser\ConvertParserProvider;

abstract class ConvertParser
{
    public static function parse(string $string) : ConvertParserProvider {
        // ToDo: Change to more SOLID method.
        return new BasicConvertParser($string);
    }
}