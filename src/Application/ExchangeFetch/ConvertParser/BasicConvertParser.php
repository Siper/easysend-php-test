<?php
/**
 * Created by PhpStorm.
 * User: siper
 * Date: 03.07.2017
 * Time: 12:49
 */

namespace Application\ExchangeFetch\ConvertParser;


use Application\Exception\ConvertParserNullValue;
use Application\ExchangeFetch\MoneyTwigExtension;
use Money\Currency;
use Money\Money;

class BasicConvertParser implements ConvertParserProvider
{
    private $sendMoney;
    private $receiveCurrency;

    /**
     * Base word to convert
     */
    const WORD_REGEX = "/convert (...) (.*\d) to (...)/i";

    /**
     * ConvertParserProvider constructor.
     * @param string $string String to parse
     */
    function __construct(string $string)
    {
        preg_match_all(self::WORD_REGEX, $string, $matches, PREG_SET_ORDER, 0);

        if ( !isset($matches[0]) ) {
            throw new ConvertParserNullValue();
        }

        $moneyIn = $matches[0][2];
        $currencyIn = $matches[0][1];
        $currencyOut = $matches[0][3];

        $formatter = new \NumberFormatter('en_US', \NumberFormatter::DECIMAL);
        $formatter->setPattern('#,##0.00;#,##0.00-');

        $moneyIn = $formatter->parse($moneyIn, \NumberFormatter::TYPE_DOUBLE) * 100;

        $this->sendMoney = new Money($moneyIn, new Currency($currencyIn));
        $this->receiveCurrency = new Currency($currencyOut);
    }

    /**
     * @return Money
     */
    public function getSendAmount(): Money
    {
        if ($this->sendMoney === null) {
            throw new ConvertParserNullValue();
        }

        return $this->sendMoney;
    }

    public function getReceiveCurrency(): Currency
    {
        if ($this->receiveCurrency === null) {
            throw new ConvertParserNullValue();
        }

        return $this->receiveCurrency;
    }
}