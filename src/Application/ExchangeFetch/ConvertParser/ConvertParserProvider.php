<?php
/**
 * Created by PhpStorm.
 * User: siper
 * Date: 03.07.2017
 * Time: 12:46
 */

namespace Application\ExchangeFetch\ConvertParser;

use Money\Currency;
use Money\Money;

interface ConvertParserProvider
{
    /**
     * ConvertParserProvider constructor.
     * @param string $string String to parse
     */
    function __construct(string $string);

    /**
     * @return Money
     */
    public function getSendAmount() : Money;
    public function getReceiveCurrency() : Currency;
}