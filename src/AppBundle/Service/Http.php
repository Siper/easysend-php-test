<?php
/**
 * Created by PhpStorm.
 * User: siper
 * Date: 05.07.2017
 * Time: 04:11
 */

namespace AppBundle\Service;


class Http
{
    /**
     * Perform HTTP Request
     * @param $url
     * @return mixed
     */
    public function performRequest($url, $is_json=false)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);

        if (!$is_json) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
        }

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec($ch);
        curl_close($ch);

        if ($is_json) {
            return json_decode($response);
        }

        return $response;
    }
}