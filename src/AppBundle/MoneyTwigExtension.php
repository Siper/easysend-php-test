<?php
/**
 * Created by PhpStorm.
 * User: siper
 * Date: 03.07.2017
 * Time: 14:08
 */

namespace AppBundle;

use Money\Currencies\ISOCurrencies;
use Money\Parser\DecimalMoneyParser;

class MoneyTwigExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('price_partial', array($this, 'pricePartialFilter')),
        );
    }

    public function pricePartialFilter($number, $whole=true)
    {
        $price = explode('.', strval($number / 100) );

        if (!isset($price[1])) {
            $price[1] = "00";
        }

        return ($whole) ? $price[0] : $price[1];
    }
}