<?php

namespace AppBundle\Controller;

use Application\Exception\ConvertParserNullValue;
use Application\Exception\CurrencyPairNotSupported;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ExchangeController extends Controller
{
    const VIEW = 'exchange/index.html.twig';

    /**
     * @Route("/exchange", name="exchange")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        return $this->render(self::VIEW);
    }

    /**
     * @Route("/exchange", name="exchange_result")
     * @Method("POST")
     */
    public function resultAction(Request $request)
    {
        $data = ($request->request->all());
        $fetcher = $this->get('exchange_fetch.fetcher');

        try {
            $fetcher->setStringToParse($data['calculationForm']['convert']);
            $dummy = $fetcher->getReceiveAmount()->getCurrency();
        } catch(ConvertParserNullValue $e) {
            $this->addFlash(
                'danger',
                'Invalid string.'
            );
            return $this->render(self::VIEW);

        } catch(CurrencyPairNotSupported $e) {
            $this->addFlash(
                'danger',
                'Typed currency pair is not supported.'
            );
            return $this->render(self::VIEW);

        }

        return $this->render(self::VIEW, [
            "results" => $fetcher
        ]);
    }
}
